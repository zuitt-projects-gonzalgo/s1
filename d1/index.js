//Array: are the kind of predefined JS Objects
let students = ["John", "Joe", "Jane", "Jessie"];

//Math: is another kind of predefined JS Objects
//No constructor.
// console.log(Math.PI);
/*console.log(Math);
console.log(Math.round(3.14));
console.log(Math.ceil(3.14));

console.log(Math.floor(Math.random() * 11));

console.log(students[students.length - 1])*/

console.log(students.slice());